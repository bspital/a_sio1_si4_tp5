
package exercices;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class TP05Exo2 {

  
    //<editor-fold defaultstate="collapsed" desc="Point d'entrée">
    /////////   FONCTION PRINCIPALE /////////////////////////////////////////
    
    public static void main(String[] args) {
        
        new   TP05Exo2().executer();
    }
    //</editor-fold>
    
    ////////////////////////////////////////////////////////// Variables globales //////////////////////////////////////////////////////////
    
 
      List<Integer> liste= new LinkedList();
      int nb = 0;
    ///////////////////////////////////////////////////////// Fin variables globales  //////////////////////////////////////////////////////
    
    public void executer(){
    
        remplirListe();
        
         // A vous de compléter
       Collections.sort(liste);
        System.out.println("La liste de tous les entiers triés par ordre croissant: \n");
        for(int i=0; i<=liste.size()-1;i++) {
            System.out.print(liste.get(i)+" ");
            if(i==12) { System.out.print("\n"); }
            if(liste.get(i)>100 && liste.get(i)<200 && liste.get(i)%2==1) {
                nb++;
            }
        }
        System.out.println("\n\nLa liste des impairs strictement compris entre 100 et 200: \n");
        System.out.print("Il y a "+nb+" nombres: ");
        for(Integer i: liste) {
            if(i>100 && i<200 && i%2==1) {
                System.out.print(i+" ");
            }
        }
        System.out.print("\n");
    }
    
    
    
    void remplirListe(){
    
        Random aleat= new Random(); 
        for (int i=1; i<=24; i++){
        
            liste.add(aleat.nextInt(300)+1);
        }
    
    
    }
}
