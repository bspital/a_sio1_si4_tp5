
package exercices;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TP05Exo4 {

  
    //<editor-fold defaultstate="collapsed" desc="Point d'entrée">
    /////////   FONCTION PRINCIPALE /////////////////////////////////////////
    
    public static void main(String[] args) {
        
        new   TP05Exo4().executer();
    }
    //</editor-fold>
    
    ////////////////////////////////////////////////////////// Variables globales //////////////////////////////////////////////////////////
    
 
    List<Long> liste= new LinkedList();
    ///////////////////////////////////////////////////////// Fin variables globales  //////////////////////////////////////////////////////
    
    public void executer(){
        liste.add(1L);
        liste.add(1L);
        long lastval = 1L;
        int stop = 0;
        while(lastval <= 1500000000L && stop == 0) {
            lastval = liste.get(liste.size()-2)+liste.get(liste.size()-1);
            if(lastval >= 1500000000L) {
                stop = 1;
            }
            else {
                liste.add(lastval);
            }
        }
        Collections.reverse(liste);
        for(int i=0; i<=liste.size()-1;i++) {
            System.out.printf("%15d",liste.get(i));
            if(i%5 == 4) { System.out.print("\n"); }
        }
        System.out.print("\n");
    }
}