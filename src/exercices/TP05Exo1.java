
package exercices;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TP05Exo1 {

  
    //<editor-fold defaultstate="collapsed" desc="Point d'entrée">
    /////////   FONCTION PRINCIPALE /////////////////////////////////////////
    
    public static void main(String[] args) {
        
        new   TP05Exo1().executer();
    }
    //</editor-fold>
    
    ////////////////////////////////////////////////////////// Variables globales //////////////////////////////////////////////////////////
    
 
      List<Integer> laListe= new LinkedList();
    
    ///////////////////////////////////////////////////////// Fin variables globales  //////////////////////////////////////////////////////
    
    public void executer(){
    
        remplirListe();
        
        // A vous de compléter
        Collections.sort(laListe);
        System.out.println("La liste de tous les entiers triés par ordre croissant: \n");
        for(int i=0; i<=laListe.size()-1;i++) {
            System.out.print(laListe.get(i)+" ");
            if(i==12) { System.out.print("\n"); }
        }
        System.out.println("\n\nLa liste des impairs strictement compris entre 100 et 200: \n");
        for(Integer i: laListe) {
            if(i>100 && i<200 && i%2==1) {
                System.out.print(i+" ");
            }
        }
        System.out.print("\n");
    }
    
   
     void remplirListe(){
    
         laListe.add(45); laListe.add(126); laListe.add(227); laListe.add(196); laListe.add(133); laListe.add(116); laListe.add(136); laListe.add(15);
         laListe.add(125); laListe.add(193);laListe.add(117); laListe.add(157); laListe.add(201); laListe.add(172); laListe.add(206); 
         laListe.add(101);laListe.add(25); laListe.add(92); laListe.add(121); laListe.add(57); laListe.add(21); laListe.add(43); laListe.add(26); 
         laListe.add(11);
    
    
    }
}
