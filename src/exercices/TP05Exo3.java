
package exercices;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TP05Exo3 {

  
    //<editor-fold defaultstate="collapsed" desc="Point d'entrée">
    /////////   FONCTION PRINCIPALE /////////////////////////////////////////
    
    public static void main(String[] args) {
        
        new   TP05Exo3().executer();
    }
    //</editor-fold>
    
    ////////////////////////////////////////////////////////// Variables globales //////////////////////////////////////////////////////////
    
    
    List<Integer> liste= new LinkedList();
    
    
    ///////////////////////////////////////////////////////// Fin variables globales  //////////////////////////////////////////////////////
    
    void executer(){
    
        remplirListe();
        
        // A vous de compléter
        System.out.println("Liste dans l'ordre inital: \n");
        for(int i=0; i<=liste.size()-1;i++) {
            System.out.printf("%4d",liste.get(i));
            if(i%10 == 9) { System.out.print("\n"); }
        }
        Collections.sort(liste);
        Collections.reverse(liste);
        System.out.println("\n\nListe triée par ordre décroissant: \n");
        for(int i=0; i<=liste.size()-1;i++) {
            System.out.printf("%4d",liste.get(i));
            if(i%10 == 9) { System.out.print("\n"); }
        }
        System.out.print("\n");
    }
    
    
    void remplirListe(){
    
         liste.add(45); liste.add(64); liste.add(227); liste.add(518); liste.add(133); liste.add(12); liste.add(22); liste.add(15);
         liste.add(125); liste.add(92); liste.add(1);liste.add(8); liste.add(157); liste.add(201); liste.add(75); liste.add(206); 
         liste.add(101);liste.add(25); liste.add(92); liste.add(888); liste.add(57); liste.add(21); liste.add(43); liste.add(26); 
         liste.add(11);
    }
    
}
