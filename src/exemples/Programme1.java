package exemples;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Programme1 {
    
    public static void main(String[] args){
  
        // Déclaration et création d'une liste vide d'entiers 
        // Cette liste est ici désignée par la variable listeDEntiers 
        
        List< Integer> listeDEntiers= new LinkedList();
        
        // Ajout d'entiers dans la liste

        listeDEntiers.add(34);
        listeDEntiers.add(12);
        listeDEntiers.add(70);
        listeDEntiers.add(22);
        listeDEntiers.add(56);
        
        //parcours de la liste avec un boucle for 
        
        // Pour chaque entier ( c'est Integer qui siginifie entier)
        // On désigne  chaque elt de la liste par une variable , ici elt
        // le symbole : signifie  "dans"
        // listeDEntiers est la variable désignant la liste
        
        System.out.print("\nCONTENU DE LA LISTE: ");
        
        for(Integer elt : listeDEntiers ){
        
            // On affiche l'element le contenu de elt
            System.out.print(elt+ " ");
        }
        
          // .size() permet d'obtenir le nombre d'élélents de la liste
        System.out.print("\n\nNOMBRE D'ELEMENTS DANS LA LISTE: ");
        System.out.println(listeDEntiers.size());
        
        
        // l'instruction Collections.min() permet d'obtenir la plus petite valeur de la liste
        System.out.print("PLUS PETITE VALEUR DE LA LISTE: ");
        System.out.println(Collections.min(listeDEntiers));
        
        // l'instruction Collections.ma() permet d'obtenir la plus grande valeur de la liste
        System.out.print("PLUS GRANDE VALEUR DE LA LISTE: ");
        System.out.println(Collections.max(listeDEntiers));
       
        System.out.println("\n"); 
    }
}


