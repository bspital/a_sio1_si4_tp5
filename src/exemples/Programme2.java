package exemples;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Programme2 {
   
    public static void main(String[] args){
   
        // Déclaration et création d'une liste vide d'entiers 
        // Cette liste est ici désignée par la variable listeDEntiers
        List<Integer> listeDEntiers= new LinkedList();
    
        // Déclaration d'un générateur de nombres aléatoires 
        // désigné par la vriable aleat
        Random aleat= new Random();
      
        // tirage au sort de la longueur de la liste entre 0 et 20
        Integer longueurListe=aleat.nextInt(21);
        
        for(int i=0;i<longueurListe;i++){
          // tirage au sort de la valeur (entre 1 et 100)de l'élément à ajouter
          listeDEntiers.add(aleat.nextInt(100)+1);
        }
  
        //tri de la liste en odre croissant
        Collections.sort(listeDEntiers);
        
        
        System.out.println("\nLISTE D'ENTIERS GENEREE:\n");
        //parcours de la liste
        for(Integer v: listeDEntiers ){
            // affichage de chaque element v
            System.out.print(v+" ");
        }
        System.out.println();
          
        // recherche de l'élément de valeur 50
        
        if(listeDEntiers.contains(50)){ // cas où l'élément de valeur 50
                                        // est dans la liste
           
            System.out.println("\n50 se trouve dans la liste");
            // On affiche où il se truve dans la liste
            System.out.print("\nIl se trouve à l'index: "+listeDEntiers.indexOf(50));
            System.out.println(" ( Rappel: le premier indice est l'indice 0 )");
        }
        else{  // cas contraire 50: n'est pas dans la liste
           
            System.out.println("\n50 ne se trouve pas dans la liste");
        }
        
        System.out.println("\nAFFICHAGE DE LA LISTE  EN ORDRE DECROISSANT:\n");
        
        // L'instruction qui suit inverse l'ordre des éléments de la liste
        Collections.reverse(listeDEntiers);
        //parcours de la liste
        for(Integer v: listeDEntiers ){
            // affichage de chaque element v
            System.out.print(v+" ");
        }
        System.out.println();
        
        
        System.out.println("\n");
    }
}

