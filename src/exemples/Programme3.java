package exemples;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Programme3 {
   
    //<editor-fold defaultstate="collapsed" desc="Point d'entrée">
    /////////   FONCTION PRINCIPALE /////////////////////////////////////////
    
    public static void main(String[] args){
        
        // On instancie la classe Programme3
        // et on appelle sa fonction executer()
        new Programme3().executer();
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //</editor-fold>
    
    /////////////////// Variables globales ///////////////////////// 
    
    // Déclaration d'une liste de chaines de caractères
    // Cette liste est initialement vide
    // La variable listeDeNoms désigne cette liste
    
    List< String > listeDeNoms= new LinkedList();
    
    // Fonction exécutée au lancement du programme 
    //( cf point d'entrée au début de ce code)
    public void executer(){
        
        // On appelle la fonction qui remplit la liste
        remplirLaListe();
           
        System.out.println("LISTE INITIALE\n");
        // On appelle la fonction qui affiche la liste
        afficherLaListe();
        
        // Cette instruction tri la liste par ordre alphabétique
        Collections.sort(listeDeNoms);
        
        System.out.println("LISTE TRIEE\n");
        // On appelle la fonction qui affiche la liste
        // dans son nouvel état, c'est à dire triée
        afficherLaListe();     
    }
    
    // Déclaration de la fonction afficherLaListe
    void afficherLaListe(){
   
        // Parcours de la liste avec une boucle indicée par i
        // listeDeNoms.size() est lae  nombre d'éléments de la liste
        for(int i=0; i<=listeDeNoms.size()-1;i++){
        
        // Affichage formatée de l'indice i
        // ainsi que du contenu de l'elémément de la liste d'indice i
        // On obtient l'élément d'indice i de la liste listeDeNoms
        // avec listeDeNoms.get(i)
        // %-15s indique l'affichage d'une chaine de caratères ( s)
        // sur 15 positions
        // le symbole - demande de cadrer à gauche    
            System.out.printf(
                
              "%2d %-15s\n",
              i,
              listeDeNoms.get(i)
            ); 
        }
        System.out.println();
    }

    // Déclaration de la fonction remplirLaListe
    void remplirLaListe() {
        
        // L'intructions listeDeNoms.add
        // permet d'ajouter un élément à la liste ListeDeNoms
        
        listeDeNoms.add("Einstein");listeDeNoms.add("Newton");listeDeNoms.add("Plank");
        listeDeNoms.add("Heisenberg");listeDeNoms.add("Curie");listeDeNoms.add("Gauss");
        listeDeNoms.add("Maxwell");listeDeNoms.add("Ampère");listeDeNoms.add("Fermi");
        listeDeNoms.add("Dirac");listeDeNoms.add("Tesla");listeDeNoms.add("Huygens");
        listeDeNoms.add("Kepler");listeDeNoms.add("Poincaré");listeDeNoms.add("Faraday");
        listeDeNoms.add("Boltzmann");listeDeNoms.add("Rutherford");listeDeNoms.add("Lavoisier");
        listeDeNoms.add("Dalton");listeDeNoms.add("Copernic");listeDeNoms.add("Kelvin");
        listeDeNoms.add("Feynman");listeDeNoms.add("Galilée");listeDeNoms.add("Meitneir");
    }    
}
