package exemples;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Programme4 {
   
    //<editor-fold defaultstate="collapsed" desc="Point d'entrée">
    /////////   FONCTION PRINCIPALE /////////////////////////////////////////
    
    public static void main(String[] args){
        
        // On instancie la classe Programme4
        // et on appelle sa fonction executer()
        new Programme4().executer();
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //</editor-fold>
    
    ///////////////// Déclaration des variables globales/////////////////////////////////////////////////////////////////
    
    // Déclaration d'un générateur de nombre aléatoires désigné par la variable genAleatoire
    Random genAleatoire=new Random();
    
    // Déclaration du clavier désigné par la variable clavier
    Scanner clavier= new Scanner(System.in);
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    
    // Déclaration d'une liste destinée à contenir des entiers ( Integer )
    List< Integer> listeDEntiers= new LinkedList<Integer>();
  
    // Fonction exécutée au lancement du programme ( cf point d'entrée au début de ce code)
    public void executer(){
   
        // Déclaration des variables locales  index et nombreSaisi de type entier (int)
        // Cette variable est locale à la fonction executer
        // Elle n'est donc visible quà l'intérieur de cette fonction
        int index=0;
        int nombreSaisi;
    
    
        
        // On appelle la fonction qui remplit la liste
        remplirLaListe();        
         
        // on appalle la fonction qui affiche les éléments de la liste
        System.out.println("LISTE NON TRIEE\n");
        afficherLaListe();
        
        // On va chercher l'élément se trouvant à l'indice 30 avec listeDEntiers.get(30)
        // et on l'affiche
        System.out.println("A l'indice  30 se trouve l'entier: "+listeDEntiers.get(30));
        System.out.println();

        // Collections.min et Collections.max
        // permettent d'obtenir le minimum et le maximum de la liste
        System.out.println("Minimum: "+Collections.min(listeDEntiers));
        System.out.println("Maximum: "+Collections.max(listeDEntiers));
        System.out.println();  
        
        
        // Cette instruction trie la liste par ordre croissant
        Collections.sort(listeDEntiers);
    
        // Appel de la fonction afficherListe()
        System.out.println("LISTE TRIEE\n");
        afficherLaListe();
        
        // Obtention de l'élément d'indice 30 de  la liste listeDEntiers 
        // grâce à la notation listeDEntiers.get(30)
        System.out.println("A l'indice 30 se trouve l'entier: "+listeDEntiers.get(30));
        System.out.println();
           
        // Saisie  au clavier grâce à l'instruction clavier.nextInt()
        // et enregistrement dans la variable nombreSaisi
        // grâce à l'instruction  nombreSaisi=clavier.nextInt();
        System.out.println("Saisir un nombre qui se trouve ou non dans la liste");
        nombreSaisi=clavier.nextInt();
        
        
        // listeDEntiers.contains permet de savoir si un élément appartient à la liste listeDEntiers
        // cette fonction retourne true ou false 
        // on peut donc  tester ce résultat directement comme ci-dessous
        // l'ecriture (listeDEntiers.contains(nombreSaisi) )
        // est équivalente à  ( listeDEntiers.contains(nombreSaisi) == true )
        if( listeDEntiers.contains(nombreSaisi) ){
        
            // ListeDEntiers.indexOf donne l'inex auquel un élément se trouve dans la liste listeDEntiers
            // en fait, l'index du premier élément qui correspond
            index = listeDEntiers.indexOf(nombreSaisi);
            // On affiche l'index trouvé
            System.out.println("Le nombre "+ nombreSaisi+ " se trouve à l'indice "+index);
            
        }  
        else System.out.println("Le nombre "+ nombreSaisi+ " ne se trouve pas dans la liste");                        
    }
       
    void afficherLaListe(){
        
        // Déclaration d'une variable index  locale à cette fonction
        // 
        int index=0;
        
        // On affiche la liste avec une boucle for each
        for(Integer val : listeDEntiers){
           
            // Affichage formaté de index et de val
            // %2d  sera remplacé à l'affichage par l'entier ( indiqué par le d de %2d ) index sur 2 positions
            // %5d sera remplacé à l'affichage par l'entier index sur 5 positions
            System.out.printf("%2d %5d\n",index,val); 
            index++; // On incrémente index  cette instruction est équivalente à index= index+1
        }
        
        System.out.println(); 
    }

    void remplirLaListe() {
        
        
        // On vide la liste si nécessaire
        listeDEntiers.clear();
        
        
        // On remplit la liste avec 50 entiers tirés au hasard entre 1 et 1000
        for(int i=0; i<50; i++){
          
            listeDEntiers.add(genAleatoire.nextInt(1000)+1);
        }    
    } 
}